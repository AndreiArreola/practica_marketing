package com.example.agencia_marketing;

import android.location.Location;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class EnvioNotificaciones extends AppCompatActivity {

    private Button General,Especifico;
    private EditText titulo,detalle,usuario;
    final String[] token = {""};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_envio_notificaciones);
        General=(Button) findViewById(R.id.btnGeneral);
        Especifico=(Button) findViewById(R.id.btnEspecifico);
        titulo=(EditText) findViewById(R.id.edtTitulo);
        detalle=(EditText) findViewById(R.id.edtDetalle);
        usuario=(EditText) findViewById(R.id.edtUsuario);
        //llamamos al metodo para la notificaion general
        General.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NotificacionGeneral();
            }
        });
        Especifico.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //verifica que el campo no este vacio
                if (TextUtils.isEmpty(usuario.getText().toString())) {
                    usuario.setError("Este campo no puede estar vacio");
                }else {
                    //si no esta vacio obtine de la base de datos todos los usuarios
                    DatabaseReference reference;
                    reference = FirebaseDatabase.getInstance().getReference();
                    reference.child("Usuarios").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            //recorre todos los hijos
                            for (DataSnapshot ds : dataSnapshot.getChildren()) {
                                //busca el usuario para mandar la notificacion
                                if(ds.getKey().equals(usuario.getText().toString())){
                                    //si se encuentra el hijo guarda el token para enviar la notificacion
                                    token[0] =ds.child("Token").getValue().toString();
                                    //llama al metodo para enviar
                                    NotificacionEspecifica();
                                }
                            }
                        }
                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                        }
                    });
                }
            }
        });
        //nos suscribimos al grupo
        FirebaseMessaging.getInstance().subscribeToTopic("atodos").addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Toast.makeText(EnvioNotificaciones.this, "Se agrego a la lista", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void NotificacionEspecifica() {
        //creamos el json para enviar
        RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());
        JSONObject json = new JSONObject();
        try {
            //vemos si en token no esta vacio
            if(!token[0].equals("")) {
                //si no lo esta creamos el cuerpo del json
                json.put("to", token[0]);
                JSONObject notificacion = new JSONObject();
                notificacion.put("titulo", titulo.getText());
                notificacion.put("detalle", detalle.getText());
                json.put("data", notificacion);
                String url = "https://fcm.googleapis.com/fcm/send";
                //creamos la peticion para la notificaion
                JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, json, null, null) {
                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> header = new HashMap<>();
                        header.put("content-type", "application/json");
                        header.put("Authorization", "key=AAAAtiH6PEI:APA91bF0nY88bgdHbpSTaun0vl6wGThFBQ6y-eMk9PC1FCD9BepxsfPPiY8RGv53SsGcIs67Yvcq05Km3QvD5emJqW2OVw5Cwgp9ayrmd7K2SzamHUwAO57nB0th7EeHP3VoUd6yg335");
                        return header;
                    }
                };
                requestQueue.add(request);
            }
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    private void NotificacionGeneral() {
        //creamos el json para la peticion
        RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());
        JSONObject json = new JSONObject();
        try {
            //agregamos a todo nuestro grupo
            json.put("to","/topics/"+"atodos");
            JSONObject notificacion=new JSONObject();
            notificacion.put("titulo", titulo.getText());
            notificacion.put("detalle", detalle.getText());
            json.put("data",notificacion);
            //creamos el cuerpo de nuestra peticion
            String url="https://fcm.googleapis.com/fcm/send";
            JsonObjectRequest request =new JsonObjectRequest(Request.Method.POST,url,json,null,null){
                @Override
                public Map<String, String> getHeaders() {
                    Map<String,String> header = new HashMap<>();
                    header.put("content-type","application/json");
                    header.put("Authorization","key=AAAAtiH6PEI:APA91bF0nY88bgdHbpSTaun0vl6wGThFBQ6y-eMk9PC1FCD9BepxsfPPiY8RGv53SsGcIs67Yvcq05Km3QvD5emJqW2OVw5Cwgp9ayrmd7K2SzamHUwAO57nB0th7EeHP3VoUd6yg335");
                    return header;
                }
            };
            requestQueue.add(request);
        }catch (JSONException e){
            e.printStackTrace();
        }
    }
}