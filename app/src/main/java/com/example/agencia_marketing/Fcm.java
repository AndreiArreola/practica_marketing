package com.example.agencia_marketing;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.annotations.NotNull;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Random;

public class Fcm extends FirebaseMessagingService {
    //Genera el token de firebase
    @Override
    public void onNewToken(@NotNull String s){
        super.onNewToken(s);
        Log.e("token","mi token es: "+s);
    }
    //almacena el token en base de datos con el nombre del usuario que se registro
    public void guardarTokenNuevo(String usuario,String token){
        DatabaseReference ref= FirebaseDatabase.getInstance().getReference().child("Usuarios");
            ref.child(usuario).child("Token").setValue(token);
    }
    //metodo para recibir las notificaciones
    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        String from = remoteMessage.getFrom();
        Log.e("TAG","Mensaje Recibido de: "+from);
        if(remoteMessage.getData().size()>0){
            Log.e("TAG","Mi titulo es "+remoteMessage.getData().get("titulo"));
            Log.e("TAG","Mi detalle es "+remoteMessage.getData().get("detalle"));
            Log.e("TAG","Mi color es "+remoteMessage.getData().get("aColor"));
            String titulo = remoteMessage.getData().get("titulo");
            String detalle = remoteMessage.getData().get("detalle");
            mayorQueOreo(titulo,detalle);
        }
    }

    //control de versiones
    private void mayorQueOreo(String titulo, String detalle) {
        String id="Mensaje";
        NotificationManager nm =(NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder=new NotificationCompat.Builder(this,id);
        if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.O){
            NotificationChannel nc = new NotificationChannel(id,"Nuevo", NotificationManager.IMPORTANCE_HIGH);
            nc.setShowBadge(true);
            assert nm !=null;
            nm.createNotificationChannel(nc);
        }
        //genera la notificacion completa para verla en la barra de notificaciones
        builder.setAutoCancel(true)
                .setWhen(System.currentTimeMillis())
                .setContentTitle(titulo)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentText(detalle)
                .setContentInfo("Nuevo");
        Random random = new Random();
        int idNotify = random.nextInt(8000);

        assert nm!=null;
        nm.notify(idNotify,builder.build());
        
    }

}
