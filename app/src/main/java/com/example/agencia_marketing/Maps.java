package com.example.agencia_marketing;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

public class Maps extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    private Double latitud=0.0,longitud=0.0;
    private FusedLocationProviderClient fusedLocationClient;
    private Button compartir,ver,notificaciones;
    private String Token;
    @Override
    public void onMapReady(GoogleMap googleMap) {
        compartir=(Button) findViewById(R.id.btnCompartir);
        ver=(Button) findViewById(R.id.btnVer);
        notificaciones=(Button) findViewById(R.id.btnNotificaciones);

        mMap = googleMap;
        //obtenemos lo permisos de ubicacion
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(Maps.this);
        if (ActivityCompat.checkSelfPermission(Maps.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(Maps.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(Maps.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,}, 1000);
        } else {
            //activamos la ubicacion y el trafico
            mMap.setMyLocationEnabled(true);
            mMap.setTrafficEnabled(true);
        }
        notificaciones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //iniciamos el activity notificaciones
                startActivity(new Intent(Maps.this,EnvioNotificaciones.class));
            }
        });
        //llamamos al metodo obtener ubicacion
        compartir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                obtenerubicacion();
            }
        });
        ver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                verusuarios();
            }
        });
        //obtenemos nuestro token
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            return;
                        }
                        // Get new Instance ID token
                        Token = task.getResult().getToken();
                    }
                });
        // Add a marker in Sydney and move the camera
    }
    private void obtenerubicacion() {
        //si el boton esta en compartir
        if ("Compartir".equals(compartir.getText())) {
            //obtenemos la ubicaion con sus permisos
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(Maps.this);
            if (ActivityCompat.checkSelfPermission(Maps.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(Maps.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(Maps.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,}, 1000);
            } else {
                fusedLocationClient.getLastLocation()
                        .addOnSuccessListener(Maps.this, new OnSuccessListener<Location>() {
                            @Override
                            public void onSuccess(Location location) {
                                if (location == null) {
                                    Toast.makeText(Maps.this, "Ubicacion no encontrada", Toast.LENGTH_SHORT).show();
                                } else {
                                    //si la encontramos la almacenamos en la base de datos
                                    latitud = location.getLatitude();
                                    longitud = location.getLongitude();

                                    SharedPreferences preferencias= getSharedPreferences("credenciales", Context.MODE_PRIVATE);
                                    String usuario=preferencias.getString("Usuario","");
                                    DatabaseReference ref= FirebaseDatabase.getInstance().getReference().child("Usuarios").child(usuario);
                                    //agregamos el token y latitud y longitud
                                    ref.child("Token").setValue(Token);
                                    ref.child("Latitud").setValue(latitud);
                                    ref.child("Longitud").setValue(longitud);
                                /*LatLng sydney = new LatLng(latitud, longitud);
                                mMap.addMarker(new MarkerOptions().position(sydney).title("Tu"));
                                mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));*/
                                }
                            }
                        });

            }
            //cambiamos el texto a no compartir
            compartir.setText("No compartir");
            //si esta en no compartir
        }else if("No compartir".equals(compartir.getText())){
            //cambiamos el texto a no compartir
            compartir.setText("Compartir");
            //borramos la latitud y longitud de la base de datos
            SharedPreferences preferencias= getSharedPreferences("credenciales", Context.MODE_PRIVATE);
            String usuario=preferencias.getString("Usuario","");
            DatabaseReference ref= FirebaseDatabase.getInstance().getReference().child("Usuarios").child(usuario);
            ref.child("Latitud").removeValue();
            ref.child("Longitud").removeValue();
        }
    }
    private void verusuarios(){
        //obtenemos nuestra localizacion
        final LatLng miubicacion = new LatLng(latitud,longitud);
        final Location location = new Location("localizacion 1");
        location.setLatitude(latitud);
        location.setLongitude(longitud);
        //si esta en ver
        if(ver.getText().equals("Ver")) {
            //obtenemos todos los usuarios de base de datos
            DatabaseReference reference;
            reference = FirebaseDatabase.getInstance().getReference();
            reference.child("Usuarios").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    //recorremos todos los usuarios
                    for (DataSnapshot ds : dataSnapshot.getChildren()) {
                        //si existe la latitud y longitud
                        if(ds.child("Latitud").exists()&&ds.child("Longitud").exists()) {
                            //creamos marcadores de los usuarios en su longitud y latitud
                            LatLng Ubicacion = new LatLng(Double.parseDouble(ds.child("Latitud").getValue().toString()), Double.parseDouble(ds.child("Longitud").getValue().toString()));
                            Location location2 = new Location("localizacion 2");
                            location2.setLatitude(Double.parseDouble(ds.child("Latitud").getValue().toString()));
                            location2.setLongitude(Double.parseDouble(ds.child("Longitud").getValue().toString()));
                            mMap.addMarker(new MarkerOptions().position(Ubicacion).title(ds.getKey()+" "+location.distanceTo(location2)+" Distancia"));
                            //cambiamos a no ver
                            ver.setText("No ver");
                        }
                    }

                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(miubicacion,10.2f));
                }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

            ver.setText("No ver");
            //si esta en no ver
        }else if(ver.getText().equals("No ver")){
            //limpiamos el mapa
            mMap.clear();
            //ubicamos el mapa en la ubicacion actual
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(miubicacion,13.2f));
            //cambiamos a ver
            ver.setText("Ver");
        }
    }
}