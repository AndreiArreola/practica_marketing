package com.example.agencia_marketing;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;



import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends AppCompatActivity {

    private TextView Crearcuenta;
    private EditText Correo;
    private EditText Contraseña;
    private Button Login;
    private String APITOKEN;
    private String Usuario;
    private String Token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();
        Crearcuenta=(TextView) findViewById(R.id.txtvcrearcuenta);
        Correo=(EditText) findViewById(R.id.edtCorreo);
        Contraseña=(EditText) findViewById(R.id.edtContrasenia);
        Login = (Button) findViewById(R.id.btnIniciarsesion);
        SharedPreferences preferencias= getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        final String token=preferencias.getString("Usuario","");
        if (token !=""){
            startActivity(new Intent(Login.this, Maps.class));
            Toast.makeText(this,"Su sesion aun es valida", Toast.LENGTH_SHORT).show();
        }
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            return;
                        }

                        // Get new Instance ID token
                        Token = task.getResult().getToken();
                    }
                });

        Crearcuenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Registro = new Intent(Login.this, Registro.class);
                startActivity(Registro);
            }
        });

        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //cechamos que los campos no esten vacios
                if (TextUtils.isEmpty(Correo.getText().toString()))
                    Correo.setError("Este campo no puede estar vacio");
                if (TextUtils.isEmpty(Contraseña.getText().toString()))
                    Contraseña.setError("Este campo no puede estar vacio");
                else{
                    //si no lo estan obtenemos todos los usuarios de la base de datos
                    DatabaseReference reference;
                    reference = FirebaseDatabase.getInstance().getReference();
                    reference.child("Usuarios").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            for (DataSnapshot ds : dataSnapshot.getChildren()) {
                                //recorremos todos los usuarios y comparamos el usuario y el password para saber si es correcto
                                if(ds.getKey().equals(Correo.getText().toString())&&ds.child("Password").getValue().equals(Contraseña.getText().toString())){
                                    //lamacenamos el usuario y lo guardamos en preferencias
                                    Usuario=Correo.getText().toString();
                                    guardarPreferencias();
                                    //creamos su token
                                    Fcm fcm=new Fcm();
                                    fcm.guardarTokenNuevo(Usuario,Token);
                                    //abrimos el mapa
                                    startActivity(new Intent(Login.this, Maps.class));
                                }
                            }
                        }
                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                        }
                    });
                }
            }
        });
    }
    private void guardarPreferencias(){
        SharedPreferences preferences = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor= preferences.edit();
        editor.putString("Token",APITOKEN);
        editor.putString("Usuario",Usuario);
        editor.commit();
    }

}
