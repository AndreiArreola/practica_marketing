<?xml version="1.0" encoding="UTF-8"?>
<root>
  <item name="android.security.keystore.KeyGenParameterSpec int getPurposes()">
    <annotation name="androidx.annotation.IntDef">
      <val name="flag" val="true" />
      <val name="value" val="{android.security.keystore.KeyProperties.PURPOSE_ENCRYPT, android.security.keystore.KeyProperties.PURPOSE_DECRYPT, android.security.keystore.KeyProperties.PURPOSE_SIGN, android.security.keystore.KeyProperties.PURPOSE_VERIFY, android.security.keystore.KeyProperties.PURPOSE_WRAP_KEY}" />
    </annotation>
  </item>
  <item name="android.security.keystore.KeyGenParameterSpec java.lang.String[] getBlockModes()">
    <annotation name="androidx.annotation.StringDef">
      <val name="value" val="{android.security.keystore.KeyProperties.BLOCK_MODE_ECB, android.security.keystore.KeyProperties.BLOCK_MODE_CBC, android.security.keystore.KeyProperties.BLOCK_MODE_CTR, android.security.keystore.KeyProperties.BLOCK_MODE_GCM}" />
    </annotation>
  </item>
  <item name="android.security.keystore.KeyGenParameterSpec java.lang.String[] getDigests()">
    <annotation name="androidx.annotation.StringDef">
      <val name="value" val="{android.security.keystore.KeyProperties.DIGEST_NONE, android.security.keystore.KeyProperties.DIGEST_MD5, android.security.keystore.KeyProperties.DIGEST_SHA1, android.security.keystore.KeyProperties.DIGEST_SHA224, android.security.keystore.KeyProperties.DIGEST_SHA256, android.security.keystore.KeyProperties.DIGEST_SHA384, android.security.keystore.KeyProperties.DIGEST_SHA512}" />
    </annotation>
  </item>
  <item name="android.security.keystore.KeyGenParameterSpec java.lang.String[] getEncryptionPaddings()">
    <annotation name="androidx.annotation.StringDef">
      <val name="value" val="{android.security.keystore.KeyProperties.ENCRYPTION_PADDING_NONE, android.security.keystore.KeyProperties.ENCRYPTION_PADDING_PKCS7, android.security.keystore.KeyProperties.ENCRYPTION_PADDING_RSA_PKCS1, android.security.keystore.KeyProperties.ENCRYPTION_PADDING_RSA_OAEP}" />
    </annotation>
  </item>
  <item name="android.security.keystore.KeyGenParameterSpec java.lang.String[] getSignaturePaddings()">
    <annotation name="androidx.annotation.StringDef">
      <val name="value" val="{android.security.keystore.KeyProperties.SIGNATURE_PADDING_RSA_PKCS1, android.security.keystore.KeyProperties.SIGNATURE_PADDING_RSA_PSS}" />
    </annotation>
  </item>
  <item name="android.security.keystore.KeyGenParameterSpec.Builder Builder(java.lang.String, int) 1">
    <annotation name="androidx.annotation.IntDef">
      <val name="flag" val="true" />
      <val name="value" val="{android.security.keystore.KeyProperties.PURPOSE_ENCRYPT, android.security.keystore.KeyProperties.PURPOSE_DECRYPT, android.security.keystore.KeyProperties.PURPOSE_SIGN, android.security.keystore.KeyProperties.PURPOSE_VERIFY, android.security.keystore.KeyProperties.PURPOSE_WRAP_KEY}" />
    </annotation>
  </item>
  <item name="android.security.keystore.KeyGenParameterSpec.Builder android.security.keystore.KeyGenParameterSpec.Builder setBlockModes(java.lang.String...) 0">
    <annotation name="androidx.annotation.StringDef">
      <val name="value" val="{android.security.keystore.KeyProperties.BLOCK_MODE_ECB, android.security.keystore.KeyProperties.BLOCK_MODE_CBC, android.security.keystore.KeyProperties.BLOCK_MODE_CTR, android.security.keystore.KeyProperties.BLOCK_MODE_GCM}" />
    </annotation>
  </item>
  <item name="android.security.keystore.KeyGenParameterSpec.Builder android.security.keystore.KeyGenParameterSpec.Builder setDigests(java.lang.String...) 0">
    <annotation name="androidx.annotation.StringDef">
      <val name="value" val="{android.security.keystore.KeyProperties.DIGEST_NONE, android.security.keystore.KeyProperties.DIGEST_MD5, android.security.keystore.KeyProperties.DIGEST_SHA1, android.security.keystore.KeyProperties.DIGEST_SHA224, android.security.keystore.KeyProperties.DIGEST_SHA256, android.security.keystore.KeyProperties.DIGEST_SHA384, android.security.keystore.KeyProperties.DIGEST_SHA512}" />
    </annotation>
  </item>
  <item name="android.security.keystore.KeyGenParameterSpec.Builder android.security.keystore.KeyGenParameterSpec.Builder setEncryptionPaddings(java.lang.String...) 0">
    <annotation name="androidx.annotation.StringDef">
      <val name="value" val="{android.security.keystore.KeyProperties.ENCRYPTION_PADDING_NONE, android.security.keystore.KeyProperties.ENCRYPTION_PADDING_PKCS7, android.security.keystore.KeyProperties.ENCRYPTION_PADDING_RSA_PKCS1, android.security.keystore.KeyProperties.ENCRYPTION_PADDING_RSA_OAEP}" />
    </annotation>
  </item>
  <item name="android.security.keystore.KeyGenParameterSpec.Builder android.security.keystore.KeyGenParameterSpec.Builder setSignaturePaddings(java.lang.String...) 0">
    <annotation name="androidx.annotation.StringDef">
      <val name="value" val="{android.security.keystore.KeyProperties.SIGNATURE_PADDING_RSA_PKCS1, android.security.keystore.KeyProperties.SIGNATURE_PADDING_RSA_PSS}" />
    </annotation>
  </item>
  <item name="android.security.keystore.KeyGenParameterSpec.Builder android.security.keystore.KeyGenParameterSpec.Builder setUserAuthenticationValidityDurationSeconds(int) 0">
    <annotation name="andro